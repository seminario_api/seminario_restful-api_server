## API FILMES

- Esté é o repositório da parte de servidor da Aplicação. Desenvolvido para o seminário de PDM.

- Trata-se de uma API de filmes, com informações como nome do Filmes, imagem do poster e descrição.

# Tecnologias

- NodeJs
- JSON
- Nodenon

# Link Apresentação

- https://youtu.be/RxsWt0AZIUY
