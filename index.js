const express = require('express');
const server = express();
const filmes = require('./src/data/filmes.json');

server.use(express.json());

server.get('/filmes', (req, res)=> {
    return res.json(filmes);
});

server.get('/filmes/:id', (req, res)=>{
    const { id } = req.params;
    const filme = filmes.find(film => film.id == id);

    if (!filme) return res.status(204).json();

    res.json(filme);
});

server.post("/filmes", (req, res)=> {
    const { name, descricao } = req.body;

  
    // salvar
  
    res.json({ name, descricao });
});

server.put("/filmes/:id", (req, res)=> {
    const { id } = req.params;
    const filme = filmes.find(film => film.id == id);
  
    if (!filmes) return res.status(204).json();
  
    const { name } = req.body;
  
    filme.name = name;
  
    res.json(filme);
});

server.delete("/filmes/:id", (req, res)=> {
    const { id } = req.params;
    const filmesFiltrados = filmes.filter(films => films.id != id);
  
    res.json(filmesFiltrados);
});

server.listen(3000, ()=>{
    console.log('Servidor esta Rodando...');
});
